package com.geekshubsacademy.cursospringedicom.jdbc;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.geekshubsacademy.cursospringedicom.CursoSpringEdicomApplication;

import lombok.Getter;

@Configuration
@EnableAutoConfiguration
public class SpringJdbcTest {
	
	@Autowired
	@Getter
	private JdbcTemplate jdbcTemplate;
	
	@Test
	public void springCreaUnContextoEInyectaJdbcTemplate() {
		// https://docs.spring.io/spring-boot/docs/1.5.10.RELEASE/reference/htmlsingle/#howto-create-a-non-web-application
		// Setup
		SpringApplication springApplication = new SpringApplication(SpringJdbcTest.class);
		springApplication.setWebEnvironment(false);
		
		// Test
		ConfigurableApplicationContext context = springApplication.run();
		
		// Assert 
		assertNull(jdbcTemplate);
		
		SpringJdbcTest test = context.getBean(SpringJdbcTest.class);
		assertNotNull(test.getJdbcTemplate());
	}

	@Test
	public void selectContraJdbcTemplate() {
		// https://docs.spring.io/spring-boot/docs/1.5.10.RELEASE/reference/htmlsingle/#howto-create-a-non-web-application
		// Setup
		SpringApplication springApplication = new SpringApplication(SpringJdbcTest.class);
		springApplication.setWebEnvironment(false);
		
		// Test
		ConfigurableApplicationContext context = springApplication.run();
		
		// Assert 
		SpringJdbcTest test = context.getBean(SpringJdbcTest.class);
		JdbcTemplate jdbcTemplate = test.getJdbcTemplate();
		Integer uno = jdbcTemplate.queryForObject("SELECT 1", Integer.class);
		assertEquals(1, uno.intValue());
	}
}
