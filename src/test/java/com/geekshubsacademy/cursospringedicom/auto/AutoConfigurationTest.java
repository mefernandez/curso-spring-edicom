package com.geekshubsacademy.cursospringedicom.auto;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.geekshubsacademy.cursospringedicom.CursoSpringEdicomApplication;

import lombok.Getter;

@Configuration
public class AutoConfigurationTest {
	
	//@Component
	public static class MyBean {
		
		public String hello() {
			return "Hi!";
		}
		
	}
	
	@Bean
	public MyBean myBean() {
		return new MyBean();
	}
	
	@Autowired
	@Getter
	private MyBean myBean;
	
	@Test
	public void springCreaUnContextoConLosBeanDefinidosEnEstaClase() {
		// https://docs.spring.io/spring-boot/docs/1.5.10.RELEASE/reference/htmlsingle/#howto-create-a-non-web-application
		// Setup
		SpringApplication springApplication = new SpringApplication(AutoConfigurationTest.class);
		springApplication.setWebEnvironment(false);
		
		// Test
		ConfigurableApplicationContext context = springApplication.run();
		
		// Assert 
		AutoConfigurationTest standalone = context.getBean(AutoConfigurationTest.class);
		MyBean myBean = standalone.getMyBean();
		assertNotNull(myBean);
		assertEquals("Hi!", myBean.hello());
	}
	

}
