package com.geekshubsacademy.cursospringedicom.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.hamcrest.Matchers.*;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.geekshubsacademy.cursospringedicom.rest.ClientesRestController;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientesRestController.class)
public class ClientesRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void obtenerClientePorId() throws Exception {
        this.mvc.perform(
        		get("/clientes/1")
        		.accept(MediaType.APPLICATION_JSON_UTF8)
        		)
        		.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nombre", is("José García")));
                
        
    }

    @Test
    public void crearCliente() throws Exception {
        this.mvc.perform(
        		post("/clientes")
        		.contentType(MediaType.APPLICATION_JSON_UTF8)
        		.content("{\"nombre\": \"Pablo Pérez\"}")
        		)
        		.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(redirectedUrl("/clientes/1"));
    }

    @Test
    public void elNombreDelClienteEsObligatorioParaCrearlo() throws Exception {
        this.mvc.perform(
        		post("/clientes")
        		.contentType(MediaType.APPLICATION_JSON_UTF8)
        		.content("{\"nombre\": null}")
        		)
        		.andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(content().string("El nombre del cliente es obligatorio"));
    }
}
