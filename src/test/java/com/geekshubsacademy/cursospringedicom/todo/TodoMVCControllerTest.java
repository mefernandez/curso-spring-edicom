package com.geekshubsacademy.cursospringedicom.todo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.geekshubsacademy.cursospringedicom.todo.TodoList;
import com.geekshubsacademy.cursospringedicom.todo.TodoMVCController;

@RunWith(SpringRunner.class)
@WebMvcTest(TodoMVCController.class)
public class TodoMVCControllerTest {

    @Autowired
    private MockMvc mvc;

//    @MockBean
//    private UserVehicleService userVehicleService;
    
    @Autowired
    private TodoMVCController controller;
    
    @Before
    public void before() {
    	controller.getTodoList().getItems().clear();
    }

    @Test
    public void itListsTwoItems() throws Exception {
    	controller.setTodoList(new TodoList("Item 1", "Item 2"));
        this.mvc.perform(
        		get("/todomvc")
        		)
        		.andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("todomvc"))
                .andExpect(model().attribute("todoList", hasProperty("items", hasSize(2))));
    }

    @Test
    public void itAddsOneItem() throws Exception {
        this.mvc.perform(
        		post("/todomvc")
        		.param("description", "Item 1")
        		)
        		.andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/todomvc"));
        
        assertEquals(1, controller.getTodoList().getItems().size());
    }
    
    @Test
    public void creatingAnItemWithNoDescriptionReturns302AndFlashAttributeWithErrors() throws Exception {
        this.mvc.perform(
        		post("/todomvc")
        		.param("description", "")
        		)
        		.andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/todomvc"))
                .andExpect(flash().attribute("ERRORS", hasSize(1)));
        
        assertEquals(1, controller.getTodoList().getItems().size());
    }
    
}
