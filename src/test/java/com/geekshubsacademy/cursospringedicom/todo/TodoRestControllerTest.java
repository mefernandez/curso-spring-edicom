package com.geekshubsacademy.cursospringedicom.todo;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.geekshubsacademy.cursospringedicom.todo.TodoList.TodoItem;

@RunWith(SpringRunner.class)
@WebMvcTest(TodoRestController.class)
public class TodoRestControllerTest {
	
    @Autowired
    private MockMvc mvc;
    
    @Autowired
    private TodoRestController controller;
    
    @Before
    public void before() {
    	this.controller.getTodoList().getItems().clear();
    	this.controller.getTodoList().setNextItemId(0);
    }
    
    @Test
    public void addItemJSON() throws Exception {
        this.mvc.perform(
        		post("/rest/todos")
        		.contentType(MediaType.APPLICATION_JSON_UTF8)
        		.content("{\"description\": \"Item JSON\"}")
        		)
        		.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.description", is("Item JSON")))
                .andExpect(jsonPath("$.done", is(false)));
    }

    @Test
    public void addItemFormData() throws Exception {
        this.mvc.perform(
        		post("/rest/todos")
        		.contentType(MediaType.MULTIPART_FORM_DATA)
        		.param("description", "Item Form Data")
        		)
        		.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.description", is("Item Form Data")))
                .andExpect(jsonPath("$.done", is(false)));
    }

    @Test
    public void addItemFormUrlEncoded() throws Exception {
        this.mvc.perform(
        		post("/rest/todos")
        		.contentType(MediaType.APPLICATION_FORM_URLENCODED)
        		.param("description", "Item Form UrlEncoded")
        		)
        		.andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.description", is("Item Form UrlEncoded")))
                .andExpect(jsonPath("$.done", is(false)));
    }

    @Test
    public void listItems() throws Exception {
    	this.controller.addItem(new TodoItem("Test Item", false));
        this.mvc.perform(
        		get("/rest/todos")
        		.accept(MediaType.APPLICATION_JSON_UTF8)
        		)
        		.andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(0)))
                .andExpect(jsonPath("$[0].description", is("Test Item")))
                .andExpect(jsonPath("$[0].done", is(false)));
    }

    @Test
    public void deleteItem() throws Exception {
    	this.controller.addItem(new TodoItem("Test Item", false));
        this.mvc.perform(
        		delete("/rest/todos/0")
        		)
        		.andDo(print())
                .andExpect(status().isNoContent());
    }

}
