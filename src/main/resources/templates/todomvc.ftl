<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TODO MVC with Spring Boot</title>
</head>
<body>

	<form method="post" action="todomvc">
		<label>Description</label>
		<input name="description" type="text">
		<button type="submit">Add</button>
	</form>
	<table>
		<thead>
			<tr>
				<th>Item Description</th>
				<th>Done</th>
			</tr>
		</thead>
		<tbody>
			<#list todoList["items"] as item>
			<tr>
				<td>${item.description}</td>
				<td><input type="checkbox" readonly="readonly" ${item.done?string('checked="checked"', '')}></td>
			</tr>
			</#list>
		</tbody>
		<tfoot></tfoot>
	</table>
</body>
</html>