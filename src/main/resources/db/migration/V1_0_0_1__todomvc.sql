create table todo_item (
	id bigint not null,
	todo_id bigint not null,
	description varchar(255) not null,
	done boolean,
	primary key (id)
);

create sequence todos_id_seq start with 1 increment by 1;
