package com.geekshubsacademy.cursospringedicom.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JDBCController {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@GetMapping("/jdbc/select")
	public ResponseEntity<String> select() {
		String uno = jdbcTemplate.queryForObject("SELECT 1", String.class);
		return ResponseEntity.ok(uno);
	}

}
