package com.geekshubsacademy.cursospringedicom;

import java.time.LocalDateTime;
import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.geekshubsacademy.cursospringedicom.aop.LogExecutionTime;

import mazz.i18n.Msg;
import mazz.i18n.Msg.BundleBaseName;
import mazz.i18n.annotation.I18NMessage;
import mazz.i18n.annotation.I18NMessages;
import mazz.i18n.annotation.I18NResourceBundle;

@SpringBootApplication
@RestController
@I18NResourceBundle( baseName = "i18n-messages" )
public class CursoSpringEdicomApplication {
	
	@I18NMessages( {
        @I18NMessage( "Hola Mundo!: {0}" ),
        @I18NMessage( value = "Hello World!: {0}",
                      locale = "en"),
        @I18NMessage( value = "Hallo Welt!: {0}",
                      locale = "de")
     })
	public static final String HOLA_MUNDO = "HOLA_MUNDO"; 

	public static void main(String[] args) {
		SpringApplication.run(CursoSpringEdicomApplication.class, args);
	}
	
	@LogExecutionTime
    @RequestMapping("/hello")
    public String home(@RequestParam(required = false) String lang) {
    	BundleBaseName baseName = new BundleBaseName("i18n-messages");
		LocalDateTime param = LocalDateTime.now();
		if (lang != null) {
    		Locale locale = Locale.forLanguageTag(lang);
			return Msg.createMsg(baseName, locale, HOLA_MUNDO, param).toString();
    	}
    	return Msg.createMsg(baseName, HOLA_MUNDO, param).toString();
        //return "Hello World!";
    }
    
    @RequestMapping("/bye")
    public String bye(@RequestParam(required = false) String lang) {
    	return "bye!";
    }

    @Bean
	public Jackson2ObjectMapperBuilderCustomizer customizeObjectMapper() {
		return new Jackson2ObjectMapperBuilderCustomizer() {

			@Override
			public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
				// Customizar ObjectMapper llamando a métodos de jacksonObjectMapperBuilder
				jacksonObjectMapperBuilder.indentOutput(true);
				jacksonObjectMapperBuilder.featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
			}
		};
	}
    
    @Bean
    public JavaTimeModule javaTimeModule() {
    	JavaTimeModule javaTimeModule = new JavaTimeModule();
		return javaTimeModule;
    }
    
    @Profile("email")
    @Configuration
    public static class EmailConfig {
    	
    	@Bean
    	public MailSender mailSender() {
    		return new JavaMailSenderImpl();
    	}
    	
    }
	
    @Profile("!email")
    @Configuration
    public static class DummyEmailConfig {
    	
    	@Bean
    	public MailSender mailSender() {
    		//return new DummyMailSender();
    		return null;
    		
    	}
    	
    }
}
