package com.geekshubsacademy.cursospringedicom.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogExecutionAspect {
	
//	@Around("@annotation(LogExecutionTime)")
//	public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
//		long start = System.currentTimeMillis();
//		System.out.println(String.format("%tFT%<tTZ: Entering %s", start, joinPoint.getSignature()));
//	    Object proceed = joinPoint.proceed();
//	    long end = System.currentTimeMillis();
//	    long executionTime = end - start;
//		System.out.println(String.format("%tFT%<tTZ: Leaving %s", end, joinPoint.getSignature()));
//	    System.out.println(String.format("Method %s executed in %d ms ",joinPoint.getSignature(), executionTime));
//	    return proceed;
//	}

	//@Pointcut("execution(* com.geekshubsacademy.cursospringedicom.*.*(..)")
	@Pointcut("execution(* com.geekshubsacademy.cursospringedicom.*.*(..))")
	public void followMyAdvice() {

	}
	
	//@Around("followMyAdvice()")
	@Around("execution(String com.geekshubsacademy.cursospringedicom.CursoSpringEdicomApplication.home(String))")
	public Object myAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
		long start = System.currentTimeMillis();
		System.out.println(String.format("%tFT%<tTZ: Entering %s", start, joinPoint.getSignature()));
	    Object proceed = joinPoint.proceed();
	    long end = System.currentTimeMillis();
	    long executionTime = end - start;
		System.out.println(String.format("%tFT%<tTZ: Leaving %s", end, joinPoint.getSignature()));
	    System.out.println(String.format("Method %s executed in %d ms ",joinPoint.getSignature(), executionTime));
	    return proceed;
	}

}
