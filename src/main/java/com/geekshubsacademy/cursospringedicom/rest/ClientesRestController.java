package com.geekshubsacademy.cursospringedicom.rest;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;
import lombok.NoArgsConstructor;

@RestController
public class ClientesRestController {
	
	@Data
	@NoArgsConstructor
	public static class Cliente {
		
		public Cliente(Long id, String nombre) {
			super();
			this.id = id;
			this.nombre = nombre;
		}

		private Long id;
		private String nombre;
		
		private LocalDateTime localDateTime = LocalDateTime.now(); 
		
	}
	
	@RequestMapping(
			method = RequestMethod.GET, 
			path = "/clientes/{id}", 
			produces = "application/json; charset=UTF-8")
	public ResponseEntity<Cliente> obtenerCliente(@PathVariable Long id) {
		return ResponseEntity.ok(new Cliente(id, "José García"));
	}
	
	@RequestMapping(
			method = RequestMethod.POST, 
			path = "/clientes", 
			consumes = "application/json; charset=UTF-8")
	public ResponseEntity<String> crearCliente(@RequestBody Cliente cliente) {
		if (cliente.getNombre() == null) {
			return ResponseEntity.badRequest().body("El nombre del cliente es obligatorio");
		}
		
		return ResponseEntity.created(URI.create("/clientes/1")).build();
	}

}
