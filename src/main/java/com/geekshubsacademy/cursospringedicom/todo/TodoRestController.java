package com.geekshubsacademy.cursospringedicom.todo;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.geekshubsacademy.cursospringedicom.todo.TodoList.TodoItem;

import lombok.Getter;
import lombok.Setter;

@RestController
public class TodoRestController {
	
	@Setter
	@Getter
	private TodoList todoList = new TodoList();
	
	@GetMapping("/rest/todos")
	public ResponseEntity<List<TodoItem>> listItems() {
		return ResponseEntity.ok(this.todoList.getItems());
	}
	
	@PostMapping("/rest/todos")
	public ResponseEntity<TodoItem> addItem(@RequestBody TodoItem item) throws URISyntaxException {
		this.todoList.add(item);
		return ResponseEntity.created(new URI("/rest/todos/" + item.getId())).body(item);
	}
	
	@GetMapping("/rest/todos/{id}")
	public ResponseEntity<TodoItem> getItem(@PathVariable int id) {
		TodoItem item = this.todoList.getItemById(id);
		if (item == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(item);
	}

	@PatchMapping("/rest/todos/{id}")
	public ResponseEntity<TodoItem> updateItem(@PathVariable int id, @RequestBody TodoItem update) {
		TodoItem item = this.todoList.getItemById(id);
		if (item == null) {
			return ResponseEntity.notFound().build();
		}
		item.setDescription(update.getDescription());
		return ResponseEntity.ok(item);
	}

	@PatchMapping("/rest/todos/{id}/done")
	public ResponseEntity<TodoItem> setDone(@PathVariable int id) {
		TodoItem item = this.todoList.getItemById(id);
		if (item == null) {
			return ResponseEntity.notFound().build();
		}
		item.setDone(true);
		return ResponseEntity.ok(item);
	}

	@PatchMapping("/rest/todos/{id}/todo")
	public ResponseEntity<TodoItem> setTodo(@PathVariable int id) {
		TodoItem item = this.todoList.getItemById(id);
		if (item == null) {
			return ResponseEntity.notFound().build();
		}
		item.setDone(false);
		return ResponseEntity.ok(item);
	}

	@DeleteMapping("/rest/todos/{id}")
	public ResponseEntity removeItem(@PathVariable int id) {
		boolean found = this.todoList.removeItemById(id);
		if (!found) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.noContent().build();
	}

	@GetMapping(path = "/rest/todos",
				params = "q")
	public ResponseEntity<List<TodoItem>> searchItems(@RequestParam(name = "q", required = false) String query) {
		if (query == null) {
			return this.listItems();
		}
		List<TodoItem> results = this.todoList.findByDescription("");
		return ResponseEntity.ok(results);
	}

	@PostMapping(path = "/rest/todos", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<TodoItem> addItemFormData(@RequestParam String description) throws URISyntaxException {
		TodoItem item = new TodoItem(description, false);
		this.todoList.add(item);
		return ResponseEntity.created(new URI("/rest/todos/" + item.getId())).body(item);
	}

	@PostMapping(path = "/rest/todos", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
	public ResponseEntity<TodoItem> addItemFormUrlEncoded(@RequestParam String description) throws URISyntaxException {
		TodoItem item = new TodoItem(description, false);
		this.todoList.add(item);
		return ResponseEntity.created(new URI("/rest/todos/" + item.getId())).body(item);
	}
}
