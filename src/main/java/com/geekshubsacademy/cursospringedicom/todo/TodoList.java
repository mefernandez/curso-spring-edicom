package com.geekshubsacademy.cursospringedicom.todo;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;

import com.geekshubsacademy.cursospringedicom.todo.TodoList.TodoItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
public class TodoList {
	
	public TodoList(String... items) {
		this.items = Arrays.stream(items)
				.map(x -> new TodoItem(x, false))
				.collect(Collectors.toList());
	}

	@Data
	@NoArgsConstructor
	@RequiredArgsConstructor
	public static class TodoItem {
		private int id;
		@NonNull
		private String description;
		@NonNull
		private boolean done;
	}
	
	private List<TodoItem> items = new ArrayList<>();
	
	private int nextItemId = 0;
	
	private int getNextItemId() {
		return nextItemId++;
	}

	public int add(TodoItem item) {
		int id = getNextItemId();
		item.setId(id);
		this.items.add(item);
		return id;
	}

	public TodoItem getItemById(final int id) {
		return this.items.stream().filter(x -> x.getId() == id).findFirst().orElse(null);
	}

	public boolean removeItemById(int id) {
		TodoItem item = getItemById(id);
		if (item == null) {
			return false;
		}
		this.items.remove(item);
		return true;
	}

	public List<TodoItem> findByDescription(String description) {
		return this.items.stream().filter(x -> x.getDescription().contains(description)).collect(Collectors.toList());
	}

}
