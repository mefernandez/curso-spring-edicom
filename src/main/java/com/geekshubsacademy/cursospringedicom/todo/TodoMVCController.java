package com.geekshubsacademy.cursospringedicom.todo;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geekshubsacademy.cursospringedicom.todo.TodoList.TodoItem;

import lombok.Getter;
import lombok.Setter;

@Controller
public class TodoMVCController {

	@Setter
	@Getter
	private TodoList todoList = new TodoList();

	@ModelAttribute
	public TodoList getTodoList() {
		return todoList;
	}

	@GetMapping("/todomvc")
	public String list() {
		// Nombre de la vista, aka el nombre del fichero en src/main/templates
		return "todomvc";
	}

	@PostMapping("/todomvc")
	public String add(@Valid TodoItem item, BindingResult result, RedirectAttributes attributes, Model model) {
		if (result.hasErrors()) {
			attributes.addFlashAttribute("ERRORS", result.getAllErrors());
			return "redirect:/todomvc";
		}
		todoList.add(item);
		
		return "redirect:/todomvc";
	}

	@InitBinder("todoItem")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new TodoItemValidator());
	}

	public static class TodoItemValidator implements Validator {

		@Override
		public boolean supports(Class clazz) {
			return TodoItem.class.equals(clazz);
		}

		@Override
		public void validate(Object obj, Errors e) {
			ValidationUtils.rejectIfEmpty(e, "description", "description.empty");
		}
	}

}
